import axios from 'axios'

// const LOCAL_URL = 'http://localhost:8081/api'
const HEROKU_URL = 'https://blooming-plains-77343.herokuapp.com/api'

export const http = axios.create({
    baseURL: HEROKU_URL
})

export default http
