export default function getErrors(errorsA) {
    const errors = {}
    for (let index = 0; index < errorsA.length; index++) {
        const element = errorsA[index];
        errors[element['param']] = element['msg']
    }
    return errors
}