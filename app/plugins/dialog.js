// https://github.com/Godofbrowser/vuejs-dialog

import Vue from 'vue';
import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
// import VuejsDialogMixin from 'vuejs-dialog/dist/vuejs-dialog-mixin.min.js'; // only needed in custom components

Vue.use(VuejsDialog, {
    html: true,
    loader: true,
    okText: 'Confirmar',
    cancelText: 'Cancelar',
    animation: 'bounce'
});

/** Ejemplo de confirm dialog (desde el store)
 *  try {
        await this._vm.$dialog.confirm('Todo ha acabado correctamente', {loader: false});
        console.log('Ok todo');
    } catch (error) {
        console.log('rechazaste el dialogo');
    }
 */
