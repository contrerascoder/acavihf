import Vue from 'vue';
import AuthControls from '../components/global/auth-control.vue';
import RedirectTo from '../components/global/redirect.vue';
import 'odometer/themes/odometer-theme-default.css';
import { mapGetters } from 'vuex';
import IOdometer from 'vue-odometer';

Vue.component('auth-controls', AuthControls)
Vue.component('redirect-to', RedirectTo)
Vue.component('io-dometer', IOdometer)

Vue.mixin({
    computed: {
        ...mapGetters('credentials', ['isLogged'])
    }
})
