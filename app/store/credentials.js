const zero = 0

export const state = () => ({
    token: '',
    user: null
})

export const mutations = {
    login(state, {token, user}) {
        state.token = token
        state.user = user
    },
    logout(state) {
        state.token = ''
        state.user = null
    }
}

export const getters = {
    isLogged(state) {
        return state.token.length > zero
    }
}
