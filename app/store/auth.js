import http from '../plugins/http';

export const state = () => ({})

export const actions = {
    async signup(ctx, payload) {
        try {
            await http.post('/auth/sign-up', payload)
            this.$toast.show('Te has registrado con exito', {duration: 3000})
            this.$router.push('/login')
        } catch (error) {
            this.$toast.show('Algo ha ido mal', {duration: 3000})
        }
    },
    async signin(ctx, payload) {
        const req = await http.post('/auth/sign-in', payload)
        this.app.store.commit('credentials/login', req.data)
    }
}
