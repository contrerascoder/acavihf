/*eslint-disable no-magic-numbers */
import getErrors from '../plugins/getErrors.js';
import http from '../plugins/http.js';

export const state = () => ({
    errors: []
})

export const mutations = {
    setErrors(state, errors) {
        state.errors = errors
    }
}

export const actions = {
    async addIncome({commit}, {incomeData, cb}) {
        commit
        const payload = {
            time: new Date(incomeData.date).getTime(),
            goods: incomeData.goods,
            health: incomeData.health,
            personal: incomeData.personal
        }
        const id = this.app.context.params.id
        try {
            const income = await http.post('/income/' + id + '/?projectId=' + id, payload)
            this.app.store.commit('stats/addIncome', income.data)
            cb()
        } catch (error) {
            const errors = JSON.parse(error.response.data.message.split('|')[1]).errors;
            commit('setErrors', getErrors(errors))
        }
    },
    async removeIncome(ctx, income) {
        await http.delete('/income/remove/' + income._id)
        this.app.store.commit('stats/removeIncome', income._id)
    }
}
