import http from '../plugins/http';

const BACK_ROUTE = -1

export const state = () => ({
    items: [],
    showArchived: false
})

export const mutations = {
    setProjects(state, items) {
        state.items = items
    },
    addProject(state, item) {
        state.items.push(item)
    },
    toggleShowArchive(state) {
        state.showArchived = !state.showArchived
    },
    editProject(state, item) {
        const FIRST_ITEM = 0
        var items = state.items
        const itemFound = items.filter(p => p._id === item._id)[FIRST_ITEM]
        items[state.items.indexOf(itemFound)].name = item.name
        state.items = items
    }
}

export const getters = {
    projects(state) {
        if (state.showArchived) {
            return state.items
        } else {
            return state.items.filter(e => e.archived === false)
        }
    }
}

export const actions = {
    async fetchProjects({commit}) {
        const req = await http.get('/projects')
        commit('setProjects', req.data)
    },
    async addProject({commit}, data) {
        const req = await http.post('/projects', data)
        commit('addProject', req.data)
        this.$router.go(BACK_ROUTE)
    },
    async editProject({commit}, data) {
        const project = {name: data.name}
        const id = this.app.context.query.projectId
        const req = await http.put('/projects/' + id, project)
        commit('editProject', req.data)
        this.$router.go(BACK_ROUTE)
    },
    async toggleArchive({commit}, data) {
        const id = data._id
        const url = `/projects/${data.archived ? 'unarchive' : 'archive'}/${id}`
        await http.put(url)
        data.archived = !data.archived
        commit('editProject', data)
    }
}