import http from '../plugins/http'

export const state = () => ({
    list: [],
    archived: false
})

export const mutations = {
    setUsers(state, usersList) {
        state.list = usersList
    },
    addUser(state, user) {
        state.list.push(user)
    },
    toggleArchived(state) {
        state.archived = !state.archived
    },
    editUser(state, user) {
        const one = 1
        var items = state.list
        const itemFound = items.find(p => p._id === user._id)
        items.splice(items.indexOf(itemFound), one, user);
        state.list = items
    }
}

export const getters = {
    users(state) {
        if (state.archived) {
            return state.list
        } else {
            return state.list.filter(e => !e.archived)
        }
    }
}

export const actions = {
    async getUsers({commit}) {
        const users = await http.get('/users')
        commit('setUsers', users.data)
    },
    async addUser({commit}, userData) {
        const user = await http.post('/users', userData)
        commit('addUser', user.data)
    },
    async updateUser({commit}, userData) {
        const user = await http.put('/users/' + userData._id, userData)
        commit('editUser', user.data)
    }
}