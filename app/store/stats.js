import http from '../plugins/http.js';
import lodash from 'lodash';

export const state = () => ({
    incomes: [],
    charges: [],
    showStats: false
})

export const mutations = {
    setData(state, {income, charges}) {
        state.incomes = income
        state.charges = charges
    },
    addCharge(state, charge) {
        state.charges.push(charge)
    },
    addIncome(state, income) {
        state.incomes.push(income)
    },
    setShowStats(state, value) {
        state.showStats = value
    },
    removeCharge(state, chargeId) {
        const uno = 1
        const charges = [...state.charges]
        const index = lodash.findIndex(state.charges, {_id: chargeId})
        charges.splice(index, uno)
        state.charges = charges
    },
    removeIncome(state, chargeId) {
        const uno = 1
        const incomes = [...state.incomes]
        const index = lodash.findIndex(state.incomes, {_id: chargeId})
        incomes.splice(index, uno)
        state.incomes = incomes
    }
}

export const getters = {
    lines(state) {
        const setType = typeLine => line => ({...line, typeLine})
        
        const lines = [
            ...state.incomes.map(setType('income')),
            ...state.charges.map(setType('charge'))
        ]
        return lodash.sortBy(lines, ['time'], ['asc'])
    },
    saldo(state) {
        const saldo = {
            goods: 0,
            health: 0,
            personal: 0,
        }
        state.incomes.forEach(e => {
            saldo.goods += e.goods
            saldo.health += e.health
            saldo.personal += e.personal
        })
        state.charges.forEach(e => {
            saldo[e.type] -= e.amount
        })
        return saldo
    }
}

export const actions = {
    async getLines({commit}, id) {
        commit('setShowStats', false)
        const req = await http.get('/stats/' + id)
        commit('setData', req.data)
        commit('setShowStats', true)
    }
}