/*eslint-disable no-magic-numbers */
import getErrors from '../plugins/getErrors.js';
import http from '../plugins/http.js';

export const state = () => ({
    message: '',
    errors: []
})

export const mutations = {
    setMessage(state, msg) {
        state.message = msg.split('|')[0]
        state.errors = getErrors(JSON.parse(msg.split('|')[1]).errors)
    }
}

export const actions = {
    async addCharge({commit}, {charge: chargeData, cb}) {
        const id = this.app.context.params.id
        try {
            const charge = chargeData.type === 'personal'
                ? await http.post('/charges/pay/' + chargeData.person + '?projectId=' + id, chargeData)
                : await http.post('/charges/addCharge?projectId=' + id, chargeData);
            this.app.store.commit('stats/addCharge', charge.data)
            commit('setMessage', '')
            cb()
        } catch (error) {
            commit('setMessage', error.response.data.message)
        }
    },
    async removeCharge(ctx, charge) {
        await http.delete('/charges/remove/' + charge._id)
        this.app.store.commit('stats/removeCharge', charge._id)
    }
}
