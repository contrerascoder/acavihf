const INDENTATION_SIZE = 4

module.exports = {
    'env': {
        'es2021': true,
        'node': true,
        'mocha': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:mocha/recommended'
    ],
    'parserOptions': {
        'ecmaVersion': 12,
        'sourceType': 'module'
    },
    'ignorePatterns': ['dist', 'server'],
    plugins: ['mocha'],
    'rules': {
        'no-magic-numbers': 'error',
        quotes: ['error', 'single', { 'allowTemplateLiterals': true }],
        'indent': ['error', INDENTATION_SIZE],
        'no-console': ['error']
    }
};
