export default {
    head: {
        titleTemplate: '%s - Finances',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        
            // hid is used as unique identifier. Do not use `vmid` for it as it will not work
            { hid: 'description', name: 'description', content: 'Meta description' }
        ],
        link: [
            {rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css'}
        ]
    },
    srcDir: 'app/',
    modules: [
        '@nuxtjs/toast',
    ],
    toast: {
        position: 'top-center',
        register: [ // Register custom toasts
            {
                name: 'my-error',
                message: 'Oops...Something went wrong',
                options: {
                    duration: 3000
                }
            }
        ]
    },
    buildModules: [
        // With options
        ['@nuxtjs/vuetify', { /* module options */ }]
    ],
    vuetify: {
        customVariables: ['~/assets/variables.scss']
    },
    plugins: [
        {src: '~/plugins/dialog.js', ssr: false},
        {src: '~/plugins/global.js', ssr: false},
        {src: '~/plugins/persistence.js', ssr: false},
    ]
}