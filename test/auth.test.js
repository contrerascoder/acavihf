import chai from 'chai'
import request from 'supertest'
import app from '../src/config/app'
import connectDB from '../src/utils/connect-db';
import httpStatus from 'http-status'
import mongoose from 'mongoose'

const data = {
    goodUser: {
        'name': 'raul',
        'surname': 'contreras',
        'email': 'dddasDdddddDqw@email.com',
        'password': 'alumdno'
    }
}

describe('Servicio de autenticación', function () {
    before(function(done) {
        connectDB().then(() => done())
    })

    it('hola mundo test', function(done) {
        request(app).get('/').expect(httpStatus.OK).then(r => {
            chai.assert(r.body, 'Hello World!')
            done();
        })
    })
    
    it('should create a user', function(done) {
        request(app).post('/api/auth/sign-up').send(data.goodUser)
            .then(r => {
                chai.assert.equal(r.status, httpStatus.CREATED)
                chai.assert.notEqual(r.body.password, data.goodUser.password)
                done()
            })
    });

    after(function(done) {
        mongoose.connection.close(() => done())
    })
});