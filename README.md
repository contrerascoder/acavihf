# ACAVIH finances
## MODELOS
Projects
 - Name
Members
 - name
 - surname
 - age
 - descripcion
Income
 - projectId
 - time
 - personalIncome
 - healthIncome
 - goodsIncome
Charges (gastos personal)
 - projectId
 - time
 - concept (si no es personal el campo type, este campo tiene que ser rellenado manualmente)
 - type (personal, health, goods)
 - memberId
 - amount

## RUTAS
members (V)
 - signUp (V)
 - signIn (V)
projects (V)
 - crud proyecto (añadir, listar, editar) (V)
Income (proyectoId)
 - addIncome
 - deposit (miembroId)
 - addCharge
Charges (proyectoId)
 - addCharge (good and health charges)
 - pay (memberId) (personal charges)