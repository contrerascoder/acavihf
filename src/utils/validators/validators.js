/* eslint-disable no-magic-numbers */
import {check} from 'express-validator'

export const name = check('name').isLength(3).withMessage('El nombre no es lo suficientemente largo')
export const surname = check('surname').isLength(7).withMessage('Los apellidos no son correctos')
export const age = check('age').isNumeric().withMessage('La edad introducida no es valida')
export const email = check('email').isEmail().withMessage('El email no es valido')
export const observations = check('observations').isLength(20).withMessage('La descripcion no es valido')

export const motivo = check('motivo').isLength(4).withMessage('El motivo no es valido')
export const atencion = check('atencion').isLength(6).withMessage('La elección no es correcta')
export const sip = check('sip').isLength(4).withMessage('La tarjeta sip no es valida') // Validar sip correctamente
export const nacionalidad = check('nacionalidad').isLength(6).withMessage('La nacionalidad no es valida')
export const localidad = check('localidad').isLength(3).withMessage('La localidad no es valida')
export const telefono = check('telefono').isLength(9).withMessage('El telefono no es valido') // validar telefono correctamente






export const validProjectName = check('name').isLength(3).withMessage('La nombre del proyecto es corto')

export const projectTime = check('time').isNumeric().withMessage('Esa fecha no es valida')
export const projectGoods = check('goods').isNumeric().withMessage('Debes poner un valor mayor que cero')
export const projectHealth = check('health').isNumeric().withMessage('Debes poner un valor mayor que cero')
export const projectPersonal = check('personal').isNumeric().withMessage('Debes poner un valor mayor que cero')

export const chargeTime = check('time').isNumeric().withMessage('Esa fecha no es valida')
export const chargeAmount = check('amount').isNumeric().withMessage('La cantidad es obligatoria')
export const chargeConcept = check('concept').isLength(4).withMessage('Ese concepto es muy corto')
export const chargeType = check('type').isLength(1).withMessage('El tipo es obligatorio')
export const chargePerson = check('person').isLength(3).withMessage('Es obligatorio poner el miembro')

export const incomeTime = check('time').isNumeric().withMessage('Esa fecha no es valida')
export const goodsAmount = check('goods').isNumeric().withMessage('La cantidad es obligatoria')
export const materialAmount = check('personal').isNumeric().withMessage('La cantidad es obligatoria')
export const healthAmount = check('health').isNumeric().withMessage('La cantidad es obligatoria')