import status from 'http-status'
import {validationResult} from 'express-validator'

/**
 * Función que mira si existen errores
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @return {void}
 */
function checkIfErrorsExists(req, res, next) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.locals = {
            info: {status: status.BAD_REQUEST}
        }
        return next(new Error('Hay algunos errorores en los datos|' + JSON.stringify(errors)))
        // return res.status(status.BAD_REQUEST).json({errors: errors.array()})
    }
    next()
}

export const createValidator = (validators) => ([
    validators, checkIfErrorsExists,
])
