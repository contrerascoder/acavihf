import jwt from 'jwt-simple';
import { SECRET } from '../config/config';

export function encode(payload) {
    return jwt.encode(payload, SECRET)
}

export function decode(token) {
    return jwt.decode(token, SECRET)
}