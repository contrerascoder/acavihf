/* eslint-disable no-console */
import mongoose from 'mongoose'
import { URIDB } from '../config/config';

mongoose.Promise = global.Promise;

/**
 * @type {mongoose.ConnectionOptions}
 */
const mongoConfig = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
}

export default function connectDB () {
    console.log('conectando a base de datos');
    return mongoose.connect(URIDB, mongoConfig);
}
