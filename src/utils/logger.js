import winston from 'winston'
import { IS_DEV, IS_PROD } from '../config/config';

export default function getLogger (service) {
    const logger = winston.createLogger({
        level: 'info',
        format: winston.format.json(),
        defaultMeta: { service: service },
        transports: [
            new winston.transports.File({ filename: 'error.log', level: 'error' }),
            new winston.transports.File({ filename: 'combined.log' }),
        ],
    });

    if (!IS_PROD && !IS_DEV) {
        logger.add(new winston.transports.Console({
            format: winston.format.simple(),
        }));
    }
    return logger;
}