import User from '../models/User'
import { decode } from '../utils/jwt'
import createLogger from '../utils/logger'

const logger = createLogger('islogged-middleware')

/** Check if a request is with authorization
* 
* @param {express.Request} req 
* @param {express.Response} res 
* @param {express.NextFunction} next 
*/
export async function isLogged(req, res, next) {
    try {
        logger.info('Check if a request is with authorization')
        if (req.headers.authorization !== '') {
            const user = decode(req.headers.authorization)
            req.user = await User.getUser(user)
            next()
        } else {
            throw new Error('Hace falta autorización para esta petición')
        }
    } catch (error) {
        next(error)
    }
}