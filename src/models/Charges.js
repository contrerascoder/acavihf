import mongoose from 'mongoose';
import IncomeModel from './Income';
import { projectModelName } from './Project';
import { memberModelName } from './User';

const POSITIVE_INCOME = 0

export const chargesModelName = 'charges'

// Declare the Schema of the Mongo model
var chargesSchema = new mongoose.Schema({
    projectId: {type: mongoose.Types.ObjectId, ref: projectModelName},
    memberId: {type: mongoose.Types.ObjectId, ref: memberModelName},
    time: {type: Number},
    concept: {type: String},
    type: {type: String, enum: ['goods', 'health', 'personal']},
    amount: {type: Number},
});

// Mirar si hay capital
chargesSchema.pre('save', async function(next) {
    const incomes = await IncomeModel.find({projectId: this.projectId})
    const isSolvent = await calcSaldo(this.type, incomes, this.amount) >= POSITIVE_INCOME
    if (isSolvent) {
        // await IncomeModel.findOneAndUpdate({projectId: this.projectId}, {$set: newIncome})
        next()
    } else {
        throw new Error('No hay suficiente saldo')
    }
})

//Export the model
export const ChargesModel = mongoose.model(chargesModelName, chargesSchema)
export default ChargesModel

async function calcSaldo(type, incomes, charge) {
    let saldo = 0, chargesAmmount = charge

    const charges = await ChargesModel.find({projectId: incomes[POSITIVE_INCOME].projectId, type: type})

    for (let index = 0; index < incomes.length; index++) {
        const income = incomes[index];
        saldo += income[type]
    }
    for (let index = 0; index < charges.length; index++) {
        const charge = charges[index];
        chargesAmmount += charge.amount
    }
    
    return saldo - chargesAmmount
}