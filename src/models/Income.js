import mongoose from 'mongoose';
import { projectModelName } from './Project';

export const incomeModelName = 'incomes'

// Declare the Schema of the Mongo model
var incomeSchema = new mongoose.Schema({
    projectId: {type: mongoose.Types.ObjectId, ref: projectModelName},
    time: {type: Number},
    personal: {type: Number},
    health: {type: Number},
    goods: {type: Number},
});

//Export the model
export const IncomeModel = mongoose.model(incomeModelName, incomeSchema)
export default IncomeModel