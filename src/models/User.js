import mongoose from 'mongoose';
import bcrypt from 'bcrypt'
import { SALTS } from '../config/config';
import { motivosContacto, tipoAtencion } from '../../userconstants';

export const memberModelName = 'members'

// Declare the Schema of the Mongo model
var memberSchema = new mongoose.Schema({
    telefono: {type: String, required: true},
    nacionalidad: {type: String, required: true},
    localidad: {type: String, required: true},
    sip: {type: String},
    atencion: {type: String, required: true, enum: tipoAtencion},
    motivo: {type: String, required: true, enum: motivosContacto},
    name:{type:String,required:true,},
    age: {type: String,required: true},
    surname:{type:String,required:true,},
    email:{type:String,required:true,unique:true,index: true},
    observations:{type:String,required:true,index: true},
    archived: {type: Boolean,default: false},
    password:{type:String,required:true,},
});

memberSchema.statics.createUser = async function createUser({name, surname, description, email, password}){
    const userQuery = await this.findOne({email: email})
    if (userQuery) {
        throw new Error('Un usuario con ese email ya existe')
    }
    
    return await this.create({
        name: name,
        surname: surname,
        email: email,
        description: description,
        password: await bcrypt.hash(password, SALTS)
    })
}

memberSchema.statics.signIn = async function singIn({email, password}){
    const user = await this.findOne({
        email: email
    });
    if (!user) throw new Error('The user not exists')

    const isEqualPassword = await bcrypt.compare(password, user.password)
    if (!isEqualPassword) throw new Error('The password is not correct')

    return {
        name: user.name,
        email: user.email,
    }
}

memberSchema.statics.getUser = async function singIn({email}){
    const user = await this.findOne({
        email: email
    });
    
    return {
        name: user.name,
        email: user.email,
    }
}


//Export the model
export const Member = mongoose.model(memberModelName, memberSchema);
export default Member;