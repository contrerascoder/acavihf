import mongoose from 'mongoose';

export const projectModelName = 'projects'

// Declare the Schema of the Mongo model
var projectSchema = new mongoose.Schema({
    name: {
        type:String,
        required:true,
        unique:true,
        index:true,
    },
    archived: {type: Boolean, default: false}
});

//Export the model
export const ProjectModel = mongoose.model(projectModelName, projectSchema)
export default ProjectModel