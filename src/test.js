/* eslint-disable */
import ChargesModel from './models/Charges';
import IncomeModel from './models/Income';
import connectDB from './utils/connect-db';
import lodash from 'lodash'

require('dotenv').config()
const zero = 0

connectDB().then(async () => {
    console.log('conectado a la base de datos');

    const projectId = '5fe22202baf8d7410cb0e118'
    const incomes = await IncomeModel.find({projectId: projectId})
    const charges = await ChargesModel.find({projectId: projectId})

    const lines = [
        ...charges,
        ...incomes,
    ]

    console.log(lodash.sortBy(lines, ['time']));

    /*console.log([
        await calcSaldo('health', incomes),
        await calcSaldo('goods', incomes),
        await calcSaldo('personal', incomes),
    ]);*/
    
    process.exit(zero)
})

async function calcSaldo(type, incomes) {
    let saldo = 0, chargesAmmount = 0

    const charges = await ChargesModel.find({projectId: incomes[zero].projectId, type: type})

    for (let index = 0; index < incomes.length; index++) {
        const income = incomes[index];
        saldo += income[type]
    }
    for (let index = 0; index < charges.length; index++) {
        const charge = charges[index];
        chargesAmmount += charge.amount
    }
    
    return saldo - chargesAmmount
}