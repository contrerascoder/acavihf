/* eslint-disable no-console */
import '@babel/polyfill'
require('dotenv/config')

import http from 'http'
import { PORT, URIDB } from '../config/config';
import connectDB from '../utils/connect-db';
import createLogger from '../utils/logger';

const logger = createLogger('app-strap')

console.log('arrancando el servidor en el puerto ' + PORT + ' y conectando a ' + URIDB);

connectDB().then(function appBootstrap () {
    logger.info(`Conectado a la base de datos ${URIDB}`)
    const app = require('../config/app').default
    http.createServer(app).listen(PORT, () => {
        logger.info('Server running at http://127.0.0.1:' + PORT);
    });
}).catch(err => {
    console.log(err); // eslint-disable-line no-console
    logger.info(`El servidor no pudo arrancar ${err.message}`)
})
