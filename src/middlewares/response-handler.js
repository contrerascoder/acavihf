/* eslint-disable no-unused-vars */
import express from 'express'

/** Error handler
 * 
 * @param {Error} err 
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */
export default function responseHandler (req, res) {
    res.status(res.locals.info.status).json(res.locals.data)
}