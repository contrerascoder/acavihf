/* eslint-disable no-unused-vars */
import express from 'express'
import statuses from 'http-status';
import { IS_PROD } from '../config/config';

const DEFAULT_STATUS = statuses.INTERNAL_SERVER_ERROR

/** Error handler
 * 
 * @param {Error} err 
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */
export default function expressErrorHandler (err, req, res, next) {
    if (!IS_PROD) {
        console.log(err); // eslint-disable-line no-console
    }
    const status = res.locals.info?.status
    res.status(status || DEFAULT_STATUS).json({
        message: err.message
    })
}