import express from 'express'
import router from '../routes'
import responseHandler from './response-handler'
import expressErrorHandler from './error-handler'
import cors from 'cors'
//import { IS_PROD } from '../config/config'

/**
 * 
 * @param {express.Application} app 
 */
function confgureAppMiddlewares (app) {
    // Pre-middlewares
    app.use([
        cors(),
        express.json(),
    ])
    // Routes middlewares
    app.use('/api', router)
    // Post middlewares
    app.use([
        responseHandler,
        expressErrorHandler
    ])
}

export default confgureAppMiddlewares