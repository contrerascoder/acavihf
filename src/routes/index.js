import express from 'express'

const router = express.Router()

router.use('/auth', require('./auth/index').default)
router.use('/projects', require('./projects/index').default)
router.use('/income/:projectId', require('./income/index').default)
router.use('/charges/', require('./charges/index').default)
router.use('/stats/', require('./stats/index').default)
router.use('/users', require('./users/index').default)

export default router