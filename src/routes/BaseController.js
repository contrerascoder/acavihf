import status from 'http-status'

export default class BaseController {
    constructor() {
        this.statuses = status;
    }

    getResponse(data, status) {
        return {data: data, info: {status: status}}
    }
}