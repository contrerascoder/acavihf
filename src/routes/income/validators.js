import { createValidator } from '../../utils/validators';
import { incomeTime, healthAmount, goodsAmount, materialAmount } from '../../utils/validators/validators';

export default createValidator([
    incomeTime, healthAmount, goodsAmount, materialAmount
])