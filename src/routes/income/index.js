import express from 'express'
import incomeController from './IncomeController'
import validator from './validators'

export const router = express.Router()

router.post('/', validator, incomeController.addIncome.bind(incomeController))
router.delete('/:id', incomeController.removeIncome.bind(incomeController))

export default router