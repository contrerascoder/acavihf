/* eslint-disable no-unused-vars */
import express from 'express'
import IncomeModel from '../../models/Income'
import log from '../../utils/logger'
import BaseController from '../BaseController'

const logger = log('income-controller')

/** IncomeController
 * Controlador para los ingresos
 * 
 */
class IncomeController extends BaseController {
    
    /** Add income
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async addIncome(req, res, next) {
        try {
            logger.info('Add income')
            const income = await IncomeModel.create({
                projectId: req.query.projectId,
                ...req.body
            })
            res.locals = {
                data: income,
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Eliminar un ingreso
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */
    async removeIncome(req, res, next) {
        try {
            logger.info('Eliminando income')
            await IncomeModel.findOneAndRemove({_id: req.params.id})
            const data = {name: 'raul', surname: 'contreras'}
            res.locals = this.getResponse(data, this.statuses.CREATED)
            next()
        } catch (error) {
            next(error)
        }
    }
}

export const incomeController = new IncomeController()
export default incomeController