import express from 'express'
import statsController from './StatsController'

export const router = express.Router()

router.get('/:projectId/', statsController.listStats.bind(statsController))

export default router