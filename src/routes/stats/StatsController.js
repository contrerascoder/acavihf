/* eslint-disable no-unused-vars */
import express from 'express'
import log from '../../utils/logger'
import BaseController from '../BaseController'
import IncomeModel from '../../models/Income';
import ChargesModel from '../../models/Charges';

const logger = log('stats-controller')

/** StatsController
 * Descripción del controller
 * 
 */
class StatsController extends BaseController {
    /** List project stats
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async listStats(req, res, next) {
        try {
            logger.info('List project stats')
            const income = await IncomeModel.find({projectId: req.params.projectId})
            const charges = await ChargesModel.find({projectId: req.params.projectId})

            res.locals = {
                data: {income, charges},
                info: {
                    status: this.statuses.OK
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }
}

export const statsController = new StatsController()

export default statsController