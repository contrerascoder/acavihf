/* eslint-disable no-unused-vars */
import express from 'express'
import ProjectModel from '../../models/Project'
import log from '../../utils/logger'
import BaseController from '../BaseController'

const logger = log('projects-controller')

/** ProjectController
 * CRU de proyectos
 * 
 * Administrar poyectos
 * 
 */
class ProjectController extends BaseController {
    /** Crear un proyecto
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async createProject(req, res, next) {
        try {
            logger.info('Crear un proyecto') 
            const project = await ProjectModel.create(req.body)
            res.locals = {
                data: project,
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Listar proyectos
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async listProjects(req, res, next) {
        try {
            logger.info('Listar proyectos') 
            const projects = await ProjectModel.find()
            res.locals = {
                data: projects,
                info: {
                    status: this.statuses.OK
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Editar proyectos
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async editProject(req, res, next) {
        try {
            logger.info('Editar proyectos')
            await ProjectModel.updateOne({_id: req.params.id}, {$set: req.body})
            res.locals = {
                data: await ProjectModel.findOne({_id: req.params.id}),
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Archive a project
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async archiveProject(req, res, next) {
        try {
            logger.info('Archive a project')
            await ProjectModel.findOneAndUpdate({_id: req.params.id}, {$set: {archived: true}})
            res.locals = {
                data: true,
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }
    /** Unarchive a project
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async unarchiveProject(req, res, next) {
        try {
            logger.info('Unrchive a project')
            await ProjectModel.findOneAndUpdate({_id: req.params.id}, {$set: {archived: false}})
            res.locals = {
                data: true,
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }
}

export const projectController = new ProjectController()
export default projectController