import { createValidator } from '../../utils/validators';
import { validProjectName } from '../../utils/validators/validators';

export default createValidator([
    validProjectName
])