import express from 'express'
import projectController from './ProjectController'
import validator from './validators'

export const router = express.Router()

router.get('/', projectController.listProjects.bind(projectController))
router.post('/', validator, projectController.createProject.bind(projectController))
router.put('/archive/:id', projectController.archiveProject.bind(projectController))
router.put('/unarchive/:id', projectController.unarchiveProject.bind(projectController))
router.put('/:id', validator, projectController.editProject.bind(projectController))

export default router