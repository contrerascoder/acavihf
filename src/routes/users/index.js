import express from 'express'
import userController from './UserController';
import userValidator from './validators'

export const router = express()

router.get('/', userController.listUsers.bind(userController))
router.post('/', userValidator, userController.createUser.bind(userController))
router.put('/:id', userController.updateUser.bind(userController))

export default router