/* eslint-disable no-unused-vars */
import express from 'express'
import BaseController from '../BaseController'
import createLogger from '../../utils/logger'
import Member from '../../models/User'
import { DEFAULT_PASSWD, SALTS } from '../../config/config'
import bcrypt from 'bcrypt'

const logger = createLogger('UserController')

/** UserController
 * Controlador de usuarios
 * 
 */
class UserController extends BaseController {
    /** Crear usuarios
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */
    async createUser(req, res, next) {
        try {
            logger.info('Crear un usuario')
            const data = await Member.create({
                ...req.body,
                password: await bcrypt.hash(DEFAULT_PASSWD, SALTS)
            })
            res.locals = this.getResponse(data, this.statuses.CREATED)
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Listar los usuarios
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */
    async listUsers(req, res, next) {
        try {
            logger.info('Listar a todos los usuarios')
            const data = await Member.find({})
            res.locals = this.getResponse(data, this.statuses.CREATED)
            next()
        } catch (error) {
            next(error)
        }
    }
    
    /** Actualizar usuarios
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */
    async updateUser(req, res, next) {
        try {
            logger.info('Actualizar un usuario')
            await Member.findOneAndUpdate({_id: req.params.id}, {$set: req.body})
            const data = await Member.findOne({_id: req.params.id})
            res.locals = this.getResponse(data, this.statuses.CREATED)
            next()
        } catch (error) {
            next(error)
        }
    }
}

export default new UserController()