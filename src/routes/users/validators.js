import { createValidator } from '../../utils/validators';
import { age, atencion, observations, email, localidad, motivo, nacionalidad, name, sip, surname, telefono } from '../../utils/validators/validators';

export default createValidator([
    name, surname, age, email, observations, motivo, atencion, sip, nacionalidad, localidad, telefono
])