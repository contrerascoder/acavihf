import express from 'express'
import { isLogged } from '../../utils/is-logged'
import userController from './UserController'

const router = express.Router()

router.post('/sign-up', userController.signUp.bind(userController))
router.post('/sign-in', userController.login.bind(userController))
router.get('/whoami', isLogged, userController.whoami.bind(userController))

export default router