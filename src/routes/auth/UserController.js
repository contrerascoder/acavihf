/* eslint-disable no-unused-vars */
import express from 'express'
import User, { Member } from '../../models/User'
import { decode, encode } from '../../utils/jwt'
import BaseController from '../BaseController'
import createLogger from '../../utils/logger'

const logger = createLogger('user-controller')

/** UserController
 * Descripción del controller
 * 
 */
class UserController extends BaseController {
    /** Sign up a user POST /api/auth/sign-up
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async signUp(req, res, next) {
        try {
            logger.info('Sign up a user')

            const user = await Member.createUser(req.body)

            res.locals = {
                data: user,
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Log in a user POST /api/auth/sign-in
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async login(req, res, next) {
        try {
            logger.info('Log in a user POST /api/auth/sign-in') 

            const user = await User.signIn(req.body)

            res.locals = {
                data: {
                    user: user,
                    token: encode(user),
                },
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Whoami GET /api/auth/whoami
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async whoami(req, res, next) {
        try {
            logger.info('Whoami GET /api/auth/whoami')
            const user = decode(req.headers.authorization)
            res.locals = {
                data: {
                    user: await User.getUser(user)
                },
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }
}

export default new UserController()