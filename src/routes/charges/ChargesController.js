/* eslint-disable no-unused-vars */
import express from 'express'
import ChargesModel, { chargesModelName } from '../../models/Charges'
import log from '../../utils/logger'
import BaseController from '../BaseController'

const logger = log('charges-service')

/** ChargesController
 * Controlador para gestionar los gastos
 * 
 */
class ChargesController extends BaseController {
    /** Suministrar un cargo
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async addCharge(req, res, next) {
        try {
            logger.info('Suministrar un cargo')
            if (req.body.type === 'personal') {
                throw new Error('No estas usando la api correcta')
            }
            const data = {
                memberId: null,
                projectId: req.query.projectId,
                ...req.body //time, concept, type, amount
            }
            const charge = await ChargesModel.create(data)
            res.locals = {
                data: charge,
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }

    /** Pagar a un socio
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    async pay(req, res, next) {
        try {
            logger.info('Suministrar un cargo')
            const data = {
                memberId: req.params.memberId,
                projectId: req.query.projectId,
                type: 'personal',
                ...req.body //time, concept, type, amount
            }
            const charge = await ChargesModel.create(data)
            res.locals = {
                data: charge,
                info: {
                    status: this.statuses.CREATED
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }
    /** Eliminar un cargo
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */
    async removeCharge(req, res, next) {
        try {
            logger.info('Eliminar un cargo')
            const data = await ChargesModel.findOne({_id: req.params.id})
            await ChargesModel.findOneAndRemove({_id: req.params.id})
            res.locals = this.getResponse(data, this.statuses.CREATED)
            next()
        } catch (error) {
            next(error)
        }
    }
}

export const controller = new ChargesController()

export default controller