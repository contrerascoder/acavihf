import express from 'express'
import controller from './ChargesController'
import { chargeValidator, payment } from './validators'

export const router = express()
router.post('/addCharge', chargeValidator, controller.addCharge.bind(controller))
router.post('/pay/:memberId', payment, controller.pay.bind(controller))
router.delete('/remove/:id', controller.removeCharge.bind(controller))

export default router