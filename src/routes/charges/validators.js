import { createValidator } from '../../utils/validators';
import { chargeAmount, chargeConcept, chargeTime, chargeType, chargePerson } from '../../utils/validators/validators';

export const chargeValidator = createValidator([
    chargeAmount, chargeConcept, chargeTime, chargeType
])

export const payment = createValidator([
    chargeAmount, chargeConcept, chargeTime, chargeType, chargePerson
])