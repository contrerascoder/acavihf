import express from 'express'
import confgureAppMiddlewares from '../middlewares'

export const app = express()

app.get('/', (req, res) => res.send('Hello World!'))

confgureAppMiddlewares(app)

export default app