const HTTP_SERVER_DEFAULT_PORT = 8081
const NUMBER_BCRYPT_SALTS = 8;

export const PORT = process.env.PORT || HTTP_SERVER_DEFAULT_PORT
export const URIDB = process.env.URIDB || 'mongodb://localhost:27017/acavih-finances'
export const IS_PROD = process.env.NODE_ENV === 'production'
export const SALTS = Number(process.env.SALTS) || NUMBER_BCRYPT_SALTS;
export const IS_DEV = process.env.NODE_ENV === 'testing'
export const SECRET = process.env.SECRET || 'DSMDSDSDSDSMsdkmdslkdqwimkdskld'
export const DEFAULT_PASSWD = process.env.DEFAULT_PASSWD || 'alumno'


// root:alumno
// mongodb+srv://root:alumno@cluster0.l1tv4.mongodb.net/acavih?retryWrites=true&w=majority