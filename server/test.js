"use strict";

var _Charges = _interopRequireDefault(require("./models/Charges"));

var _Income = _interopRequireDefault(require("./models/Income"));

var _connectDb = _interopRequireDefault(require("./utils/connect-db"));

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

require('dotenv').config();

var zero = 0;
(0, _connectDb["default"])().then( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
  var projectId, incomes, charges, lines;
  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          console.log('conectado a la base de datos');
          projectId = '5fe22202baf8d7410cb0e118';
          _context.next = 4;
          return _Income["default"].find({
            projectId: projectId
          });

        case 4:
          incomes = _context.sent;
          _context.next = 7;
          return _Charges["default"].find({
            projectId: projectId
          });

        case 7:
          charges = _context.sent;
          lines = [].concat(_toConsumableArray(charges), _toConsumableArray(incomes));
          console.log(_lodash["default"].sortBy(lines, ['time']));
          /*console.log([
              await calcSaldo('health', incomes),
              await calcSaldo('goods', incomes),
              await calcSaldo('personal', incomes),
          ]);*/

          process.exit(zero);

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
})));

function calcSaldo(_x, _x2) {
  return _calcSaldo.apply(this, arguments);
}

function _calcSaldo() {
  _calcSaldo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(type, incomes) {
    var saldo, chargesAmmount, charges, index, income, _index, charge;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            saldo = 0, chargesAmmount = 0;
            _context2.next = 3;
            return _Charges["default"].find({
              projectId: incomes[zero].projectId,
              type: type
            });

          case 3:
            charges = _context2.sent;

            for (index = 0; index < incomes.length; index++) {
              income = incomes[index];
              saldo += income[type];
            }

            for (_index = 0; _index < charges.length; _index++) {
              charge = charges[_index];
              chargesAmmount += charge.amount;
            }

            return _context2.abrupt("return", saldo - chargesAmmount);

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _calcSaldo.apply(this, arguments);
}