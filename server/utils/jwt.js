"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encode = encode;
exports.decode = decode;

var _jwtSimple = _interopRequireDefault(require("jwt-simple"));

var _config = require("../config/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function encode(payload) {
  return _jwtSimple["default"].encode(payload, _config.SECRET);
}

function decode(token) {
  return _jwtSimple["default"].decode(token, _config.SECRET);
}