"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = connectDB;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _config = require("../config/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_mongoose["default"].Promise = global.Promise;
/**
 * @type {mongoose.ConnectionOptions}
 */

var mongoConfig = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
};

function connectDB() {
  return _mongoose["default"].connect(_config.URIDB, mongoConfig);
}