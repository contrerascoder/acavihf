"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getLogger;

var _winston = _interopRequireDefault(require("winston"));

var _config = require("../config/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getLogger(service) {
  var logger = _winston["default"].createLogger({
    level: 'info',
    format: _winston["default"].format.json(),
    defaultMeta: {
      service: service
    },
    transports: [new _winston["default"].transports.File({
      filename: 'error.log',
      level: 'error'
    }), new _winston["default"].transports.File({
      filename: 'combined.log'
    })]
  });

  if (!_config.IS_PROD && !_config.IS_DEV) {
    logger.add(new _winston["default"].transports.Console({
      format: _winston["default"].format.simple()
    }));
  }

  return logger;
}