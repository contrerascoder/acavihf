"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isLogged = isLogged;

var _User = _interopRequireDefault(require("../models/User"));

var _jwt = require("../utils/jwt");

var _logger = _interopRequireDefault(require("../utils/logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var logger = (0, _logger["default"])('islogged-middleware');
/** Check if a request is with authorization
* 
* @param {express.Request} req 
* @param {express.Response} res 
* @param {express.NextFunction} next 
*/

function isLogged(_x, _x2, _x3) {
  return _isLogged.apply(this, arguments);
}

function _isLogged() {
  _isLogged = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
    var user;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            logger.info('Check if a request is with authorization');

            if (!(req.headers.authorization !== '')) {
              _context.next = 10;
              break;
            }

            user = (0, _jwt.decode)(req.headers.authorization);
            _context.next = 6;
            return _User["default"].getUser(user);

          case 6:
            req.user = _context.sent;
            next();
            _context.next = 11;
            break;

          case 10:
            throw new Error('Hace falta autorización para esta petición');

          case 11:
            _context.next = 16;
            break;

          case 13:
            _context.prev = 13;
            _context.t0 = _context["catch"](0);
            next(_context.t0);

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 13]]);
  }));
  return _isLogged.apply(this, arguments);
}