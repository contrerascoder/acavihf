"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.router = void 0;

var _express = _interopRequireDefault(require("express"));

var _StatsController = _interopRequireDefault(require("./StatsController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

exports.router = router;
router.get('/:projectId/', _StatsController["default"].listStats.bind(_StatsController["default"]));
var _default = router;
exports["default"] = _default;