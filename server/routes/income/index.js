"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.router = void 0;

var _express = _interopRequireDefault(require("express"));

var _IncomeController = _interopRequireDefault(require("./IncomeController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

exports.router = router;
router.post('/', _IncomeController["default"].addIncome.bind(_IncomeController["default"]));
router["delete"]('/:id', _IncomeController["default"].removeIncome.bind(_IncomeController["default"]));
var _default = router;
exports["default"] = _default;