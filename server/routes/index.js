"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

router.use('/auth', require('./auth/index')["default"]);
router.use('/projects', require('./projects/index')["default"]);
router.use('/income/:projectId', require('./income/index')["default"]);
router.use('/charges/', require('./charges/index')["default"]);
router.use('/stats/', require('./stats/index')["default"]);
router.use('/users', require('./users/index')["default"]);
var _default = router;
exports["default"] = _default;