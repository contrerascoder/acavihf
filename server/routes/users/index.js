"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.router = void 0;

var _express = _interopRequireDefault(require("express"));

var _UserController = _interopRequireDefault(require("./UserController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express["default"])();
exports.router = router;
router.get('/', _UserController["default"].listUsers.bind(_UserController["default"]));
router.post('/', _UserController["default"].createUser.bind(_UserController["default"]));
router.put('/:id', _UserController["default"].updateUser.bind(_UserController["default"]));
var _default = router;
exports["default"] = _default;