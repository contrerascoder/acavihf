"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _BaseController2 = _interopRequireDefault(require("../BaseController"));

var _logger = _interopRequireDefault(require("../../utils/logger"));

var _User = _interopRequireDefault(require("../../models/User"));

var _config = require("../../config/config");

var _bcrypt = _interopRequireDefault(require("bcrypt"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var logger = (0, _logger["default"])('UserController');
/** UserController
 * Controlador de usuarios
 * 
 */

var UserController = /*#__PURE__*/function (_BaseController) {
  _inherits(UserController, _BaseController);

  var _super = _createSuper(UserController);

  function UserController() {
    _classCallCheck(this, UserController);

    return _super.apply(this, arguments);
  }

  _createClass(UserController, [{
    key: "createUser",

    /** Crear usuarios
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */
    value: function () {
      var _createUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
        var data;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                logger.info('Crear un usuario');
                _context.t0 = _User["default"];
                _context.t1 = _objectSpread;
                _context.t2 = _objectSpread({}, req.body);
                _context.t3 = {};
                _context.next = 8;
                return _bcrypt["default"].hash(_config.DEFAULT_PASSWD, _config.SALTS);

              case 8:
                _context.t4 = _context.sent;
                _context.t5 = {
                  password: _context.t4
                };
                _context.t6 = (0, _context.t1)(_context.t2, _context.t3, _context.t5);
                _context.next = 13;
                return _context.t0.create.call(_context.t0, _context.t6);

              case 13:
                data = _context.sent;
                res.locals = this.getResponse(data, this.statuses.CREATED);
                next();
                _context.next = 21;
                break;

              case 18:
                _context.prev = 18;
                _context.t7 = _context["catch"](0);
                next(_context.t7);

              case 21:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 18]]);
      }));

      function createUser(_x, _x2, _x3) {
        return _createUser.apply(this, arguments);
      }

      return createUser;
    }()
    /** Listar los usuarios
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */

  }, {
    key: "listUsers",
    value: function () {
      var _listUsers = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res, next) {
        var data;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                logger.info('Listar a todos los usuarios');
                _context2.next = 4;
                return _User["default"].find({});

              case 4:
                data = _context2.sent;
                res.locals = this.getResponse(data, this.statuses.CREATED);
                next();
                _context2.next = 12;
                break;

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](0);
                next(_context2.t0);

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 9]]);
      }));

      function listUsers(_x4, _x5, _x6) {
        return _listUsers.apply(this, arguments);
      }

      return listUsers;
    }()
    /** Actualizar usuarios
         * 
         * @param {express.Request} req 
         * @param {express.Response} res 
         * @param {express.NextFunction} next 
         */

  }, {
    key: "updateUser",
    value: function () {
      var _updateUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res, next) {
        var data;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                logger.info('Actualizar un usuario');
                _context3.next = 4;
                return _User["default"].findOneAndUpdate({
                  _id: req.params.id
                }, {
                  $set: req.body
                });

              case 4:
                _context3.next = 6;
                return _User["default"].findOne({
                  _id: req.params.id
                });

              case 6:
                data = _context3.sent;
                res.locals = this.getResponse(data, this.statuses.CREATED);
                next();
                _context3.next = 14;
                break;

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);
                next(_context3.t0);

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 11]]);
      }));

      function updateUser(_x7, _x8, _x9) {
        return _updateUser.apply(this, arguments);
      }

      return updateUser;
    }()
  }]);

  return UserController;
}(_BaseController2["default"]);

var _default = new UserController();

exports["default"] = _default;