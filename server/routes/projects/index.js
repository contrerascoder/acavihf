"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.router = void 0;

var _express = _interopRequireDefault(require("express"));

var _ProjectController = _interopRequireDefault(require("./ProjectController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

exports.router = router;
router.get('/', _ProjectController["default"].listProjects.bind(_ProjectController["default"]));
router.post('/', _ProjectController["default"].createProject.bind(_ProjectController["default"]));
router.put('/archive/:id', _ProjectController["default"].archiveProject.bind(_ProjectController["default"]));
router.put('/unarchive/:id', _ProjectController["default"].unarchiveProject.bind(_ProjectController["default"]));
router.put('/:id', _ProjectController["default"].editProject.bind(_ProjectController["default"]));
var _default = router;
exports["default"] = _default;