"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.projectController = void 0;

var _express = _interopRequireDefault(require("express"));

var _Project = _interopRequireDefault(require("../../models/Project"));

var _logger = _interopRequireDefault(require("../../utils/logger"));

var _BaseController2 = _interopRequireDefault(require("../BaseController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var logger = (0, _logger["default"])('projects-controller');
/** ProjectController
 * CRU de proyectos
 * 
 * Administrar poyectos
 * 
 */

var ProjectController = /*#__PURE__*/function (_BaseController) {
  _inherits(ProjectController, _BaseController);

  var _super = _createSuper(ProjectController);

  function ProjectController() {
    _classCallCheck(this, ProjectController);

    return _super.apply(this, arguments);
  }

  _createClass(ProjectController, [{
    key: "createProject",

    /** Crear un proyecto
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */
    value: function () {
      var _createProject = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
        var project;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                logger.info('Crear un proyecto');
                _context.next = 4;
                return _Project["default"].create(req.body);

              case 4:
                project = _context.sent;
                res.locals = {
                  data: project,
                  info: {
                    status: this.statuses.CREATED
                  }
                };
                next();
                _context.next = 12;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context["catch"](0);
                next(_context.t0);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 9]]);
      }));

      function createProject(_x, _x2, _x3) {
        return _createProject.apply(this, arguments);
      }

      return createProject;
    }()
    /** Listar proyectos
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */

  }, {
    key: "listProjects",
    value: function () {
      var _listProjects = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res, next) {
        var projects;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                logger.info('Listar proyectos');
                _context2.next = 4;
                return _Project["default"].find();

              case 4:
                projects = _context2.sent;
                res.locals = {
                  data: projects,
                  info: {
                    status: this.statuses.OK
                  }
                };
                next();
                _context2.next = 12;
                break;

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](0);
                next(_context2.t0);

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 9]]);
      }));

      function listProjects(_x4, _x5, _x6) {
        return _listProjects.apply(this, arguments);
      }

      return listProjects;
    }()
    /** Editar proyectos
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */

  }, {
    key: "editProject",
    value: function () {
      var _editProject = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res, next) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                logger.info('Editar proyectos');
                _context3.next = 4;
                return _Project["default"].updateOne({
                  _id: req.params.id
                }, {
                  $set: req.body
                });

              case 4:
                _context3.next = 6;
                return _Project["default"].findOne({
                  _id: req.params.id
                });

              case 6:
                _context3.t0 = _context3.sent;
                _context3.t1 = {
                  status: this.statuses.CREATED
                };
                res.locals = {
                  data: _context3.t0,
                  info: _context3.t1
                };
                next();
                _context3.next = 15;
                break;

              case 12:
                _context3.prev = 12;
                _context3.t2 = _context3["catch"](0);
                next(_context3.t2);

              case 15:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 12]]);
      }));

      function editProject(_x7, _x8, _x9) {
        return _editProject.apply(this, arguments);
      }

      return editProject;
    }()
    /** Archive a project
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */

  }, {
    key: "archiveProject",
    value: function () {
      var _archiveProject = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res, next) {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                logger.info('Archive a project');
                _context4.next = 4;
                return _Project["default"].findOneAndUpdate({
                  _id: req.params.id
                }, {
                  $set: {
                    archived: true
                  }
                });

              case 4:
                res.locals = {
                  data: true,
                  info: {
                    status: this.statuses.CREATED
                  }
                };
                next();
                _context4.next = 11;
                break;

              case 8:
                _context4.prev = 8;
                _context4.t0 = _context4["catch"](0);
                next(_context4.t0);

              case 11:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 8]]);
      }));

      function archiveProject(_x10, _x11, _x12) {
        return _archiveProject.apply(this, arguments);
      }

      return archiveProject;
    }()
    /** Unarchive a project
     * 
     * @param {express.Request} req 
     * @param {express.Response} res 
     * @param {express.NextFunction} next 
     */

  }, {
    key: "unarchiveProject",
    value: function () {
      var _unarchiveProject = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res, next) {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                logger.info('Unrchive a project');
                _context5.next = 4;
                return _Project["default"].findOneAndUpdate({
                  _id: req.params.id
                }, {
                  $set: {
                    archived: false
                  }
                });

              case 4:
                res.locals = {
                  data: true,
                  info: {
                    status: this.statuses.CREATED
                  }
                };
                next();
                _context5.next = 11;
                break;

              case 8:
                _context5.prev = 8;
                _context5.t0 = _context5["catch"](0);
                next(_context5.t0);

              case 11:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 8]]);
      }));

      function unarchiveProject(_x13, _x14, _x15) {
        return _unarchiveProject.apply(this, arguments);
      }

      return unarchiveProject;
    }()
  }]);

  return ProjectController;
}(_BaseController2["default"]);

var projectController = new ProjectController();
exports.projectController = projectController;
var _default = projectController;
exports["default"] = _default;