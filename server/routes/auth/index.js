"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _isLogged = require("../../utils/is-logged");

var _UserController = _interopRequireDefault(require("./UserController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

router.post('/sign-up', _UserController["default"].signUp.bind(_UserController["default"]));
router.post('/sign-in', _UserController["default"].login.bind(_UserController["default"]));
router.get('/whoami', _isLogged.isLogged, _UserController["default"].whoami.bind(_UserController["default"]));
var _default = router;
exports["default"] = _default;