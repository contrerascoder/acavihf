"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.router = void 0;

var _express = _interopRequireDefault(require("express"));

var _ChargesController = _interopRequireDefault(require("./ChargesController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express["default"])();
exports.router = router;
router.post('/addCharge', _ChargesController["default"].addCharge.bind(_ChargesController["default"]));
router.post('/pay/:memberId', _ChargesController["default"].pay.bind(_ChargesController["default"]));
router["delete"]('/remove/:id', _ChargesController["default"].removeCharge.bind(_ChargesController["default"]));
var _default = router;
exports["default"] = _default;