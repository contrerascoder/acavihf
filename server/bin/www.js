"use strict";

require("@babel/polyfill");

var _http = _interopRequireDefault(require("http"));

var _config = require("../config/config");

var _connectDb = _interopRequireDefault(require("../utils/connect-db"));

var _logger = _interopRequireDefault(require("../utils/logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require('dotenv/config');

var logger = (0, _logger["default"])('app-strap');
(0, _connectDb["default"])().then(function appBootstrap() {
  logger.info("Conectado a la base de datos ".concat(_config.URIDB));

  var app = require('../config/app')["default"];

  _http["default"].createServer(app).listen(_config.PORT, function () {
    logger.info('Server running at http://127.0.0.1:' + _config.PORT);
  });
})["catch"](function (err) {
  if (!_config.IS_PROD) {
    console.log(err); // eslint-disable-line no-console
  }

  logger.info("El servidor no pudo arrancar ".concat(err.message));
});