"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _config = require("../config/config");

var _cors = _interopRequireDefault(require("cors"));

var _routes = _interopRequireDefault(require("../routes"));

var _responseHandler = _interopRequireDefault(require("./response-handler"));

var _errorHandler = _interopRequireDefault(require("./error-handler"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * 
 * @param {express.Application} app 
 */
function confgureAppMiddlewares(app) {
  // Pre-middlewares
  app.use([!_config.IS_PROD && (0, _cors["default"])(), _express["default"].json()]); // Routes middlewares

  app.use('/api', _routes["default"]); // Post middlewares

  app.use([_responseHandler["default"], _errorHandler["default"]]);
}

var _default = confgureAppMiddlewares;
exports["default"] = _default;