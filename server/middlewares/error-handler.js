"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = expressErrorHandler;

var _express = _interopRequireDefault(require("express"));

var _httpStatus = _interopRequireDefault(require("http-status"));

var _config = require("../config/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* eslint-disable no-unused-vars */
var DEFAULT_STATUS = _httpStatus["default"].INTERNAL_SERVER_ERROR;
/** Error handler
 * 
 * @param {Error} err 
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */

function expressErrorHandler(err, req, res, next) {
  var _res$locals$info;

  if (!_config.IS_PROD) {
    console.log(err); // eslint-disable-line no-console
  }

  var status = (_res$locals$info = res.locals.info) === null || _res$locals$info === void 0 ? void 0 : _res$locals$info.status;
  res.status(status || DEFAULT_STATUS).json({
    message: err.message
  });
}