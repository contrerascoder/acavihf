"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = responseHandler;

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* eslint-disable no-unused-vars */

/** Error handler
 * 
 * @param {Error} err 
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */
function responseHandler(req, res) {
  res.status(res.locals.info.status).json(res.locals.data);
}