"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.Member = exports.memberModelName = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _config = require("../config/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var memberModelName = 'members'; // Declare the Schema of the Mongo model

exports.memberModelName = memberModelName;
var memberSchema = new _mongoose["default"].Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  description: {
    type: String,
    required: true,
    index: true
  },
  archived: {
    type: Boolean,
    "default": false
  },
  password: {
    type: String,
    required: true
  }
});

memberSchema.statics.createUser = /*#__PURE__*/function () {
  var _createUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref) {
    var name, surname, description, email, password, userQuery;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            name = _ref.name, surname = _ref.surname, description = _ref.description, email = _ref.email, password = _ref.password;
            _context.next = 3;
            return this.findOne({
              email: email
            });

          case 3:
            userQuery = _context.sent;

            if (!userQuery) {
              _context.next = 6;
              break;
            }

            throw new Error('Un usuario con ese email ya existe');

          case 6:
            _context.t0 = this;
            _context.t1 = name;
            _context.t2 = surname;
            _context.t3 = email;
            _context.t4 = description;
            _context.next = 13;
            return _bcrypt["default"].hash(password, _config.SALTS);

          case 13:
            _context.t5 = _context.sent;
            _context.t6 = {
              name: _context.t1,
              surname: _context.t2,
              email: _context.t3,
              description: _context.t4,
              password: _context.t5
            };
            _context.next = 17;
            return _context.t0.create.call(_context.t0, _context.t6);

          case 17:
            return _context.abrupt("return", _context.sent);

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  function createUser(_x) {
    return _createUser.apply(this, arguments);
  }

  return createUser;
}();

memberSchema.statics.signIn = /*#__PURE__*/function () {
  var _singIn = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref2) {
    var email, password, user, isEqualPassword;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            email = _ref2.email, password = _ref2.password;
            _context2.next = 3;
            return this.findOne({
              email: email
            });

          case 3:
            user = _context2.sent;

            if (user) {
              _context2.next = 6;
              break;
            }

            throw new Error('The user not exists');

          case 6:
            _context2.next = 8;
            return _bcrypt["default"].compare(password, user.password);

          case 8:
            isEqualPassword = _context2.sent;

            if (isEqualPassword) {
              _context2.next = 11;
              break;
            }

            throw new Error('The password is not correct');

          case 11:
            return _context2.abrupt("return", {
              name: user.name,
              email: user.email
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  function singIn(_x2) {
    return _singIn.apply(this, arguments);
  }

  return singIn;
}();

memberSchema.statics.getUser = /*#__PURE__*/function () {
  var _singIn2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(_ref3) {
    var email, user;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            email = _ref3.email;
            _context3.next = 3;
            return this.findOne({
              email: email
            });

          case 3:
            user = _context3.sent;
            return _context3.abrupt("return", {
              name: user.name,
              email: user.email
            });

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  function singIn(_x3) {
    return _singIn2.apply(this, arguments);
  }

  return singIn;
}(); //Export the model


var Member = _mongoose["default"].model(memberModelName, memberSchema);

exports.Member = Member;
var _default = Member;
exports["default"] = _default;