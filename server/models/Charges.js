"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.ChargesModel = exports.chargesModelName = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _Income = _interopRequireDefault(require("./Income"));

var _Project = require("./Project");

var _User = require("./User");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var POSITIVE_INCOME = 0;
var chargesModelName = 'charges'; // Declare the Schema of the Mongo model

exports.chargesModelName = chargesModelName;
var chargesSchema = new _mongoose["default"].Schema({
  projectId: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _Project.projectModelName
  },
  memberId: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _User.memberModelName
  },
  time: {
    type: Number
  },
  concept: {
    type: String
  },
  type: {
    type: String,
    "enum": ['goods', 'health', 'personal']
  },
  amount: {
    type: Number
  }
}); // Mirar si hay capital

chargesSchema.pre('save', /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(next) {
    var incomes, isSolvent;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _Income["default"].find({
              projectId: this.projectId
            });

          case 2:
            incomes = _context.sent;
            _context.next = 5;
            return calcSaldo(this.type, incomes, this.amount);

          case 5:
            _context.t0 = _context.sent;
            _context.t1 = POSITIVE_INCOME;
            isSolvent = _context.t0 >= _context.t1;

            if (!isSolvent) {
              _context.next = 12;
              break;
            }

            // await IncomeModel.findOneAndUpdate({projectId: this.projectId}, {$set: newIncome})
            next();
            _context.next = 13;
            break;

          case 12:
            throw new Error('No hay suficiente saldo');

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}()); //Export the model

var ChargesModel = _mongoose["default"].model(chargesModelName, chargesSchema);

exports.ChargesModel = ChargesModel;
var _default = ChargesModel;
exports["default"] = _default;

function calcSaldo(_x2, _x3, _x4) {
  return _calcSaldo.apply(this, arguments);
}

function _calcSaldo() {
  _calcSaldo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(type, incomes, charge) {
    var saldo, chargesAmmount, charges, index, income, _index, _charge;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            saldo = 0, chargesAmmount = charge;
            _context2.next = 3;
            return ChargesModel.find({
              projectId: incomes[POSITIVE_INCOME].projectId,
              type: type
            });

          case 3:
            charges = _context2.sent;

            for (index = 0; index < incomes.length; index++) {
              income = incomes[index];
              saldo += income[type];
            }

            for (_index = 0; _index < charges.length; _index++) {
              _charge = charges[_index];
              chargesAmmount += _charge.amount;
            }

            return _context2.abrupt("return", saldo - chargesAmmount);

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _calcSaldo.apply(this, arguments);
}