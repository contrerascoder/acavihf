"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.ProjectModel = exports.projectModelName = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var projectModelName = 'projects'; // Declare the Schema of the Mongo model

exports.projectModelName = projectModelName;
var projectSchema = new _mongoose["default"].Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  archived: {
    type: Boolean,
    "default": false
  }
}); //Export the model

var ProjectModel = _mongoose["default"].model(projectModelName, projectSchema);

exports.ProjectModel = ProjectModel;
var _default = ProjectModel;
exports["default"] = _default;