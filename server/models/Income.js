"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.IncomeModel = exports.incomeModelName = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _Project = require("./Project");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var incomeModelName = 'incomes'; // Declare the Schema of the Mongo model

exports.incomeModelName = incomeModelName;
var incomeSchema = new _mongoose["default"].Schema({
  projectId: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _Project.projectModelName
  },
  time: {
    type: Number
  },
  personal: {
    type: Number
  },
  health: {
    type: Number
  },
  goods: {
    type: Number
  }
}); //Export the model

var IncomeModel = _mongoose["default"].model(incomeModelName, incomeSchema);

exports.IncomeModel = IncomeModel;
var _default = IncomeModel;
exports["default"] = _default;