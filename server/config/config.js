"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT_PASSWD = exports.SECRET = exports.IS_DEV = exports.SALTS = exports.IS_PROD = exports.URIDB = exports.PORT = void 0;
var HTTP_SERVER_DEFAULT_PORT = 8081;
var NUMBER_BCRYPT_SALTS = 8;
var PORT = process.env.PORT || HTTP_SERVER_DEFAULT_PORT;
exports.PORT = PORT;
var URIDB = process.env.URIDB || 'mongodb://localhost:27017/testserver';
exports.URIDB = URIDB;
var IS_PROD = process.env.NODE_ENV === 'production';
exports.IS_PROD = IS_PROD;
var SALTS = Number(process.env.SALTS) || NUMBER_BCRYPT_SALTS;
exports.SALTS = SALTS;
var IS_DEV = process.env.NODE_ENV === 'testing';
exports.IS_DEV = IS_DEV;
var SECRET = process.env.SECRET || 'DSMDSDSDSDSMsdkmdslkdqwimkdskld';
exports.SECRET = SECRET;
var DEFAULT_PASSWD = process.env.DEFAULT_PASSWD || 'alumno';
exports.DEFAULT_PASSWD = DEFAULT_PASSWD;